exports.config = {
  framework: 'jasmine',

  seleniumAddress: 'http://172.30.30.26:4444/wd/hub',
  //seleniumAddress: 'http://selenium:4444/wd/hub',

  specs: ['todo-spec.js'],

  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
            args: [ "--headless", "--disable-gpu", "--window-size=800,600" ]
    }
  },

  plugins: [{
      package: 'protractor-screenshoter-plugin',
      screenshotPath: './REPORTS/e2e',
      screenshotOnExpect: 'failure+success',
      screenshotOnSpec: 'none',
      withLogs: 'true',
      writeReportFreq: 'asap',
      clearFoldersBeforeTest: true
  }],

  onPrepare: function() {
      // returning the promise makes protractor wait for the reporter config before executing tests
      return global.browser.getProcessedConfig().then(function(config) {
          //it is ok to be empty
      });
  }
};
